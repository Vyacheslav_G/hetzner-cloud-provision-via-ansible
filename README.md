---

## Provision to Hetzner-Cloud

In your bitbucket account or repository Settings add Environment variable HCLOUD_TOKEN (for connection to Hezner-cloud)

1. Click on last commit in repository
2. Click on "Run pipeline" on the right
3. Chose Pipeline "custom:provistion-to-hetzner" in drop down list and press "Run" button

For connection to Hetzner-Cloud [thetechnick/hcloud-ansible](https://github.com/thetechnick/hcloud-ansible) is used.

